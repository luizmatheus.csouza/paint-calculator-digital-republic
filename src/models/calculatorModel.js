class Area {
  constructor(widthWall, heightWall, quantityDoor, quantityWindow) {
    this.widthWall = widthWall;
    this.heightWall = heightWall;
    this.quantityDoor = quantityDoor;
    this.quantityWindow = quantityWindow;
  }
}

module.exports = Area;