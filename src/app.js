const express = require('express');
const app = express();
const calculatorRoutes = require('./routes/calculatorRoutes');
const errorHandler = require('./middlewares/middleware');
const cors = require('cors')

app.use(cors())
app.use(express.json());
app.use('/calculate', calculatorRoutes);
app.use(errorHandler);

module.exports = app;