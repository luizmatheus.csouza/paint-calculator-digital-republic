const Area = require('../models/calculatorModel');
const calculationService = require('../services/calculationService')

exports.calculate = async (req, res) => {
  try {
    const wallData = req.body;

    if(wallData.length != 4){
      return res.status(400).json({ error: "Invalid number of walls" })
    }

    const walls = wallData.map((data) => new Area(data.widthWall, data.heightWall, data.quantityDoor, data.quantityWindow))

    for(const wall of walls){
      const validator = wall.widthWall * wall.heightWall;

      if(validator < 1 || validator > 50){
        return res.status(400).json({ error: "Invalid wall dimensions" })
      }

    }

    
    const resultado = await calculationService.calculate(walls);
    
    
    res.status(201).json(resultado);
  } catch (error) {
    console.log(error)
    res.status(400).send(error)
  }
}