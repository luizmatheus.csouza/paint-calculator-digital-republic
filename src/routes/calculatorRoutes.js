const express = require('express');
const router = express.Router();
const calculationController = require('../controllers/calculationController')

router.post('/', calculationController.calculate)

module.exports = router;