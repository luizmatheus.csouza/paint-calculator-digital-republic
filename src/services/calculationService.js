const CustomError = require("../error/customError");


exports.calculate = async (walls) => {

  let sizeCan18 = 0;
  let sizeCan3_6 = 0;
  let sizeCan2_5 = 0;
  let sizeCan0_5 = 0;
  
  let restLiter = 0

  const areaDoor = 1.52
  const areaWindow = 2.4

  let totalAreaWall = 0
  
  const calcultatorArea = walls.map(wall => {

    const wallWidth = wall.widthWall
    const wallHeight = wall.heightWall
    const doorQuantity = wall.quantityDoor
    const windowQuantity = wall.quantityWindow

    const totalAreaDoorAndWindow = (doorQuantity*areaDoor)+(windowQuantity*areaWindow)

    const areaWall = (wallWidth * wallHeight)-totalAreaDoorAndWindow

    if(totalAreaDoorAndWindow > areaWall/2){
      throw new CustomError(400,'As portas e janelas não podem ocupar mais que 50% da área da parede. Insira valores válidos')
    }

    
    if(doorQuantity>0 && wallHeight<2.2){
      throw new CustomError(400, 'A altura da parede tem que ser no mínimo 30 centímetros maior que a porta. Insira valores válidos')
    }

    totalAreaWall += areaWall

  })

  const liters = totalAreaWall/5

  let count = liters

  while(count>0){
    if(count>=18){
      sizeCan18++
      count = count-18
    }else if(count>=3.6){
      sizeCan3_6++
      count = count-3.6
    }else if(count>=2.5){
      sizeCan2_5++
      count = count-2.5
    }else if(count>=0.5){
      sizeCan0_5++
      count = count-0.5
    }else{
      sizeCan0_5++
      restLiter = (0.5-count).toFixed(2)
      count = 0;
    }
  }

  return ({sizeCan18, sizeCan3_6, sizeCan2_5, sizeCan0_5, restLiter, totalAreaWall, liters})

};