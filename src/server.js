const app = require('./app');
const config = require('./config/config');

app.listen(config.port, () => {
  console.log(`Microserviço rodando em http://localhost:${config.port}`);
});