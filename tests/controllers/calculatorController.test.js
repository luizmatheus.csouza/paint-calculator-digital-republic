const { calculate } = require('../../src/controllers/calculationController');
const Area = require('../../src/models/calculatorModel');
const calculationService = require('../../src/services/calculationService');

jest.mock('../../src/services/calculationService'); // Mocking the calculationService

describe('calculate', () => {
  let req, res;

  beforeEach(() => {
    req = {
      body: []
    };

    res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
      send: jest.fn()
    };

    calculationService.calculate.mockClear();
  });

  test('should return 400 if the number of walls is not 4', async () => {
    req.body = [{ width: 2, height: 3, door: 1, window: 1 }];

    await calculate(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ error: 'Invalid number of walls' });
  });

  test('should return 400 if wall dimensions are invalid', async () => {
    req.body = [
      { width: 0.4, height: 2, door: 1, window: 1 },
      { width: 5, height: 2, door: 1, window: 1 },
      { width: 2, height: 1, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 }
    ];

    await calculate(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ error: 'Invalid wall dimensions' });
  });

  test('should return 201 and the calculation result for valid input', async () => {
    req.body = [
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 }
    ];

    const result = [{ size18: 1, size3: 2, size2: 3, size0: 4 }];
    calculationService.calculate.mockResolvedValue(result);

    await calculate(req, res);

    expect(calculationService.calculate).toHaveBeenCalledWith(expect.any(Array));
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith(result);
  });

  test('should handle errors and return 400', async () => {
    req.body = [
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 },
      { width: 2, height: 3, door: 1, window: 1 }
    ];

    const error = new Error('Test error');
    calculationService.calculate.mockRejectedValue(error);

    await calculate(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.send).toHaveBeenCalledWith(error);
  });
});
